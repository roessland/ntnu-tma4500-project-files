%close all
%clear all
%% Single-phase compressible AD solver
% The purpose of the example is to give the first introduction to how one
% can use the automatic differentiation (AD) class in MRST to write a flow
% simulator for a compressible single-phase model. For simplicity, the
% reservoir is assumed to be a rectangular box with homogeneous properties
% and no-flow boundaries. Starting from a hydrostatic initial state, the
% reservoir is produced from a vertical well that will create a zone of
% pressure draw-down. As more fluids are produced, the average pressure in
% the reservoir drops, causing a gradual decay in the production rate.

%% Set up model geometry
N = 32;
G = cartGrid([N, N], [N, N]);
G = computeGeometry(G);

%% Define rock model
rock.perm = repmat(0.3*milli*darcy, [G.cells.num, 1]);
rock.poro = repmat(0.3, [G.cells.num, 1]);
cr   = 1e-4/barsa;
p_r  = 200*barsa;
pv_r = poreVolume(G, rock);
pv   = @(p) pv_r .* exp( cr * (p - p_r) );

%% Define model for compressible fluid
mu    = 5*centi*poise;
c     = 1e-3/barsa;
rho_r = 850*kilogram/meter^3;
rho   = @(p) rho_r .* exp( c * (p - p_r) );

%% Assume a single horizontal well in the corner
cellInx = sub2ind(G.cartDims, 1, 1);
W = addWell([ ], G, rock, cellInx, 'Name', 'P1', 'Dir', 'z');

% Initial pressure in reservoir is the reference pressure
p_init = repmat(p_r, [G.cells.num, 1]);

%% Compute transmissibilities
N  = double(G.faces.neighbors);
intInx = all(N ~= 0, 2);
N  = N(intInx, :);                          % Interior neighbors
hT = computeTrans(G, rock);                 % Half-transmissibilities
cf = G.cells.faces(:,1);
nf = G.faces.num;
T  = 1 ./ accumarray(cf, 1 ./ hT, [nf, 1]); % Harmonic average
T  = T(intInx);                             % Restricted to interior

%% Define discrete operators
n = size(N,1);
C = sparse( [(1:n)'; (1:n)'], N, ones(n,1)*[-1 1], n, G.cells.num);
grad = @(x)C*x;
div  = @(x)-C'*x;
avg  = @(x) 0.5 * (x(N(:,1)) + x(N(:,2)));

%% Define flow equations
v      = @(p)  -(T/mu).*( grad(p) );
presEq = @(p,p0,dt) (1/dt)*(pv(p).*rho(p) - pv(p0).*rho(p0)) ...
                      + div( avg(rho(p)).*v(p) );
%% Define well equations
wc = W(1).cells; % connection grid cells
WI = W(1).WI;    % well productivity indices

% The flow into the well from the well cell, due to the pressure difference
% between the well and the average pressure in the well cell. The Peaceman
% approximation is used.
q_conn  = @(p,bhp) WI .* (rho(p(wc)) / mu) .* (bhp - p(wc));

% bhp is the borehole-pressure, and is constantly 100 bars.
ctrlEq = @(bhp)       bhp-100*barsa;

%% Initialize for solution loop
[p_ad, bhp_ad] = initVariablesADI(p_init, p_init(wc(1)));
nc = G.cells.num;
[pIx, bhpIx] = deal(1:nc, nc+1);

numSteps = 52;                  % number of time-steps
totTime  = 365*day;             % total simulation time
dt       = totTime / numSteps;  % constant time step
tol      = 1e-5;                % Newton tolerance
maxits   = 10;                  % max number of Newton its

sol = repmat(struct('time',[],'pressure',[]),[numSteps+1,1]);
sol(1)  = struct('time', 0, 'pressure', double(p_ad)); 

%% Main loop
t = 0; step = 0;
while t < totTime,
   t = t + dt;
   step = step + 1;
   fprintf('\nTime step %d: Time %.2f -> %.2f days\n', ...
      step, convertTo(t - dt, day), convertTo(t, day));
   % Newton loop
   resNorm = 1e99;
   p0  = double(p_ad); % Previous step pressure
   nit = 0;
   while (resNorm > tol) && (nit <= maxits)
      % Add source terms to homogeneous pressure equation:
      eq1     = presEq(p_ad, p0, dt);
      eq1(wc) = eq1(wc) - q_conn(p_ad, bhp_ad);

      % Collect all equations
      eqs = {eq1, ctrlEq(bhp_ad)};
      % Concatenate equations and solve for update:
      eq  = cat(eqs{:});
      J   = eq.jac{1};  % Jacobian
      res = eq.val;     % residual
      upd = -(J \ res); % Newton update
      % Update variables
      p_ad.val   = p_ad.val   + upd(pIx);
      bhp_ad.val = bhp_ad.val + upd(bhpIx);

      resNorm = norm(res);
      nit     = nit + 1;
      fprintf('  Iteration %3d:  Res = %.4e\n', nit, resNorm);
   end

   if nit > maxits,
      error('Newton solves did not converge')
   else % store solution
      sol(step+1)  = struct('time', t, 'pressure', double(p_ad));
   end
end

p_ad.val

%% Plot pressure decay
figure
plot(...
   [sol(2:end).time]/day, mean([sol(2:end).pressure]/barsa));
xlabel('time [days]');
ylabel('avg pressure [bar]');

%% Plot pressure evolution
figure;
steps = [2 9 14 21];
for i=1:4
   subplot(2,2,i);
   
   plotCellData(G, sol(steps(i)).pressure/barsa, 'EdgeColor','none');
   grid off
   %plotWell(G,W);
   %view(-125,20), camproj perspective
   caxis([115 205]);
   axis tight off; zoom(1.0)
   text(200,170,-8,[num2str(round(steps(i)*dt/day)) ' days'],'FontSize',14);
end

h=colorbar('horiz','Position',[.1 .05 .8 .025]);
colormap(jet(256));

%{
Copyright 2009-2015 SINTEF ICT, Applied Mathematics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
