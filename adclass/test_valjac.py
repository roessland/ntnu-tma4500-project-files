from __future__ import division
import unittest
import numpy as np
from valjac import ValJac, vstack, dot, multiply, exp
from numpy.testing import assert_approx_equal

class TestOperators(unittest.TestCase):

    def test_init(self):
        # Test a basic column vector with Jacobian
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        assert np.array_equal(x.val, np.matrix([[3], [2]]))
        assert np.array_equal(x.jac, np.matrix([[2, 0], [0, 1]]))

        # Input variables not matrices
        with self.assertRaises(AssertionError):
            x = ValJac(np.matrix(5), 2)
        with self.assertRaises(AssertionError):
            x = ValJac(5, np.matrix(2))

        # Wrong amount of rows in Jacobian
        with self.assertRaises(AssertionError):
            x = ValJac(np.matrix([[3], [2]]), np.matrix([[1, 2], [3, 4], [5, 6]]))

        # Numpy array instead of matrix
        with self.assertRaises(AssertionError):
            x = ValJac(np.matrix([[1], [2]]), np.eye(2))

        # Default identity Jacobian
        x = ValJac(np.matrix([[5]]))
        assert x.val == 5
        assert x.jac[0,0] == 1

    def test_negation(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        y = -x
        assert isinstance(y, ValJac)
        assert np.array_equal(y.val, np.matrix([[-3], [-2]]))
        assert np.array_equal(y.jac, np.matrix([[-2, 0], [0, -1]]))

    def test_addition_valjac_valjac(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        y = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        z = x + y
        assert isinstance(z, ValJac)
        assert np.array_equal(z.val, np.matrix([[6], [4]]))
        assert np.array_equal(z.jac, np.matrix([[4, 0], [0, 2]]))

    def test_addition_constant_vector(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        y = np.matrix("1; 2")
        z = x + y # using __add__
        assert isinstance(z, ValJac)
        assert np.array_equal(z.val, np.matrix([[4], [4]]))
        assert np.array_equal(z.jac, np.matrix([[2, 0], [0, 1]]))

    def test_raddition_constant_vector(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        y = np.matrix("1; 2")
        with self.assertRaises(NotImplementedError):
            z = y + x # using __radd__

    def test_subtraction_constant_vector(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        y = np.matrix("1; 2")
        z = x - y
        assert isinstance(z, ValJac)
        assert np.array_equal(z.val, np.matrix([[2], [0]]))
        assert np.array_equal(z.jac, np.matrix([[2, 0], [0, 1]]))

    def test_len(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0, 3], [5, 6, 7]]))
        assert len(x) == 2

    def test_getitem(self):
        # Two derivatives wrt x1, x2
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0], [0, 1]]))
        x0 = x[0]
        assert x0.val == 3
        assert np.array_equal(x0.jac, np.matrix([[2, 0]]))

        # Three derivatives wrt x1, x2, x3
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0, 5], [0, 1, 5]]))
        x0 = x[0]
        assert x0.val == 3
        assert np.array_equal(x0.jac, np.matrix([[2, 0, 5]]))

    def test_getitem_range(self):
        x = ValJac(np.matrix([[3], [2], [4]]), np.matrix([[2, 0, 5], [0, 1, 5], [1, 2, 3]]))
        ix = [0, 1]
        y1 = x[ix]
        y2 = ValJac(np.matrix([[3], [2]]), np.matrix([[2, 0, 5], [0, 1, 5]]))
        assert np.allclose(y1.val, y2.val)
        assert np.allclose(y1.jac, y2.jac)

    def test_setitem(self):
        x = ValJac(np.matrix([[3], [2], [4]]), np.matrix([[2, 0, 5], [0, 1, 5], [1, 2, 3]]))
        w = ValJac(np.matrix([[9]]), np.matrix([[9, 9, 9]]))
        x[0] = w
        assert np.allclose(x[0].val, w.val)
        assert np.allclose(x[0].jac, w.jac)

    def test_setitem_indices(self):
        x = ValJac(np.matrix([[3], [2], [4]]), np.matrix([[2, 0, 5], [0, 1, 5], [1, 2, 3]]))
        y = ValJac(np.matrix([[2], [4], [4]]), np.matrix([[0, 1, 5], [1, 2, 3], [1, 2, 3]]))
        x[[0,1]] = x[[1,2]]
        assert np.allclose(x.val, y.val)
        assert np.allclose(x.jac, y.jac)

    def test_eq_with_valjac(self):
        """Equality between ValJac vectors should compare the values only."""
        x = ValJac(np.matrix([[3.], [2.]]))
        y = ValJac(np.matrix([[3.], [2.]]), np.matrix([[1, 3], [1, 1]]))
        assert (x == y).all()

    def test_eq_with_constant_vector(self):
        x = np.matrix([[3.], [2.]])
        y = ValJac(np.matrix([[3.], [1.]]), np.matrix([[1, 3], [1, 1]]))
        assert np.array_equal(x == y, np.matrix([[True], [False]]))

    def test_multiplication_by_scalar(self):
        """
        Multiplication by a scalar should simply multiply both the value and
        the Jacobian with the scalar.
        """
        x = ValJac(np.matrix([[3], [2]]))
        y1 = 3*x
        y2 = x*3
        Ey = ValJac(np.matrix([[3*3], [3*2]]), np.matrix([[3*1, 0], [0, 3*1]]))

        assert np.array_equal(y1.val, Ey.val)
        assert np.array_equal(y1.jac, Ey.jac)
        assert np.array_equal(y2.val, Ey.val)
        assert np.array_equal(y2.jac, Ey.jac)

    def test_multiplication_single_variable(self):
        x = ValJac(np.matrix([[3]]), np.matrix([[2]]))
        y = ValJac(np.matrix([[4]]), np.matrix([[2]]))
        z = x*y
        assert isinstance(z, ValJac)
        assert z.val == np.matrix(3*4)
        assert z.jac == np.matrix(2*4 + 3*2) # (xy)' = x'y + xy'

    def test_left_multiplication_with_matrix(self):
        # First a simple dot product
        # [2 4 6](u, Ju) = 2(u,Ju)[0] + 4(u,Ju)[1] + 6(u,Ju)[2]
        A = np.matrix([[2, 4, 6]])
        x = ValJac(np.matrix([[1], [2], [3]]), np.matrix([[2,1,0],[0,1,2],[3,0,1]]))
        y1 = dot(A, x)
        assert isinstance(y1, ValJac)
        y2 = 2*x[0] + 4*x[1] + 6*x[2]
        assert np.array_equal(y1.val, y2.val)
        assert np.array_equal(y1.jac, y2.jac)

        # And then a matrix dot product
        # [2 4 6]           [2(u,Ju)[0] + 4(u,Ju)[1] + 6(u,Ju)[2]]
        # [3 2 1]*(u, Ju) = [3(u,Ju)[0] + 2(u,Ju)[1] + 1(u,Ju)[2]]
        # [1 1 1]           [1(u,Ju)[0] + 1(u,Ju)[1] + 1(u,Ju)[2]]
        A = np.matrix([[2, 4, 6], [3, 2, 1], [1, 1, 1]])
        y1 = dot(A, x)
        y2_1 = 2*x[0] + 4*x[1] + 6*x[2]
        y2_2 = 3*x[0] + 2*x[1] + 1*x[2]
        y2_3 = 1*x[0] + 1*x[1] + 1*x[2]
        y2 = vstack([y2_1, y2_2, y2_3])
        assert np.array_equal(y1.val, y2.val)
        assert np.array_equal(y1.jac, y2.jac)

    def test_multiplication_elementwise(self):
        x = ValJac(np.matrix([[1], [2], [3]]),   np.matrix([[2,1,0],[0,1,2],[3,0,1]]))
        k = np.matrix([[3],[4],[5]])
        y1 = multiply(k, x)
        y2 = ValJac(np.matrix([[3], [8], [15]]), np.matrix([[6,3,0],[0,4,8],[15,0,5]]))
        assert np.array_equal(y1.val, y2.val)
        assert np.array_equal(y1.jac, y2.jac)

    def test_multiply_valjac(self):
        # Elementwise multiplication like .* in MATLAB
        # u = [3x^2, x+y]^T
        # v = [3x+y^2, y^2 + x^2]^T
        # multiply(u, v) = [(3x^2)(3x+y^2),  (x+y)(y^2 + x^2)]^T
        u = ValJac(np.matrix([[12], [5]]), np.matrix([[12, 0], [1, 1]]))
        v = ValJac(np.matrix([[15], [13]]), np.matrix([[3, 6], [4, 6]]))
        w1 = multiply(u, v)
        w2 = ValJac(np.matrix([[180], [65]]), np.matrix([[216, 72], [33, 43]]))
        assert np.array_equal(w1.val, w2.val)
        assert np.array_equal(w1.jac, w2.jac)

    def test_divide_by_scalar(self):
        x = ValJac(np.matrix([[3.], [6.]]), np.matrix([[1., 2.], [3., 4.]]))
        y = x / 3.
        assert isinstance(y, ValJac)
        assert np.array_equal(y.val, np.matrix([[1.], [2.]]))
        assert np.array_equal(y.jac, np.matrix([[1./3, 2./3], [3./3, 4./3]]))

    def test_scalar_divided_by_valjac(self):
        x = ValJac(np.matrix([[3.], [6.]]), np.matrix([[1., 2.], [3., 4.]]))
        y = 3. / x
        y0 = y[0]
        assert y0.val == 1.
        print(-3./3**2 * x.jac[0,0])
        assert_approx_equal(y0.jac[0,0], -3/3**2 * x.jac[0,0])
        assert_approx_equal(y0.jac[0,1], -3/3**2 * x.jac[0,1])

    def test_vstack_valjac_only(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[1, 0], [0, 1]]))
        y = ValJac(np.matrix([[4], [3]]), np.matrix([[2, 2], [2, 2]]))
        z = vstack([x, y])
        assert np.array_equal(z.val, np.matrix("3; 2; 4; 3"))
        assert np.array_equal(z.jac, np.matrix("1 0; 0 1; 2 2; 2 2"))

        a = ValJac(np.matrix([[1], [2], [3]]),
                   np.matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]]))
        b = vstack([a[0], a[1], a[2]])
        assert isinstance(b, ValJac)
        assert np.array_equal(a.val, b.val)
        assert np.array_equal(a.jac, b.jac)

    def test_vstack_constant_scalar(self):
        x = 5
        y = 3
        z = vstack([x, y])
        assert isinstance(z, np.ndarray)
        assert np.array_equal(z, np.array([[5], [3]]))

    def test_vstack_valjac_constant(self):
        y0 = ValJac(np.matrix([[5], [2]]), np.matrix([[1, 0,4], [0, 1,4]]))
        y1 = np.matrix([[1], [1], [5]])
        y = vstack([y0, y1])

        assert np.array_equal(y.val, np.matrix([[5], [2], [1], [1], [5]]))
        assert np.array_equal(y.jac, np.matrix([[1,0,4],
                                                [0,1,4],
                                                [0,0,0],
                                                [0,0,0],
                                                [0,0,0]]))

    def test_pow_nonsquare(self):
        x = ValJac(np.matrix([[3], [2]]), np.matrix([[1, 0], [0, 1]]))
        with self.assertRaises(NotImplementedError):
            x**3

    def test_pow_square(self):
        x = ValJac(np.matrix([[5]]), np.matrix([[3, 2]]))
        y = x**2
        assert_approx_equal(y.val[0,0], 5**2)
        assert_approx_equal(y.jac[0,0], 2*5**1 * 3)
        assert_approx_equal(y.jac[0,1], 2*5**1 * 2)

    def test_exp_scalar(self):
        assert_approx_equal(exp(5), np.exp(5))

    def test_exp_vector(self):
        x = np.matrix([[1], [1.2], [3]])
        y1 = exp(x)
        y2 = np.exp(x)
        assert np.allclose(y1, y2)

    def test_exp_valjac(self):
        # exp([2x; x+y]), x=1, y=2
        x = ValJac(np.matrix([[2], [3]]), np.matrix([[2, 0], [1, 1]]))
        y = exp(x)
        y_val = np.exp(x.val)
        y_jac = np.matrix([[2*np.exp(1), 0], [np.exp(3), np.exp(3)]])
        assert isinstance(y, ValJac)
        assert np.allclose(y.val, y_val)



class TestRosenbrock(unittest.TestCase):

    @staticmethod
    def rosenbrock(x):
        """
        The Rosenbrock test function for multiple dimensions, as described here:
        http://en.wikipedia.org/wiki/Test_functions_for_optimization
        """
        n = len(x)

        S = 0.0
        for i in range(0, n-1):
            S = 100 * (x[i+1] - x[i]**2)**2 + (x[i] - 1)**2 + S
        return S

    @staticmethod
    def rosenbrock_residual(x):
        n = len(x)
        l = list()

        # df/dx0
        l.append(  200*(x[1] - x[0]**2) * (-2*x[0]) +  2*(x[0] - 1)  )

        # df/dxi
        for i in range(1, n-1):
            l.append(  200*(x[i] - x[i-1]**2) + 200*(x[i+1] - x[i]**2)*(-2*x[i]) + 2*(x[i] - 1)  )

        # df/dx(n-1)
        l.append(  200*(x[n-1] - x[n-2]**2)  )

        return vstack(l)

    def test_rosenbrock_gradient_R2(self):
        """
        Test that it can find the gradient of the Rosenbrock function.
        """
        x0 = 3
        x1 = 2
        x = ValJac(np.matrix([[x0], [x1]]), np.matrix([[1, 0], [0, 1]]))
        y = self.rosenbrock(x)

        f = 100*(x1 - x0**2)**2 + (x0 - 1)**2
        dfdx0 = 200*(x1 - x0**2)*(-2*x0) + 2*(x0-1)
        dfdx1 = 200*(x1-x0**2)

        assert_approx_equal(y.val[0,0], f)
        assert_approx_equal(y.jac[0,0], dfdx0)
        assert_approx_equal(y.jac[0,1], dfdx1)

    def test_rosenbrock_gradient_Rn(self):
        """
        Test that we can find the residual of the Rosenbrock function using the
        AD class. (And also that the residual function is correct)
        """
        x0 = (1, 2, 3, 4, 5)
        n = len(x0)
        x = np.matrix(x0).T
        x_ad = ValJac(x)

        y_ad = self.rosenbrock(x_ad).jac
        y    = self.rosenbrock_residual(x).T

        assert np.array_equal(y_ad, y)


if __name__ == '__main__':
    unittest.main()
