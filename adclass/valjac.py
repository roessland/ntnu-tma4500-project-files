# -*- coding: utf-8 -*-
from __future__ import division
from exceptions import NotImplementedError

import numpy as np
import sympy as sp

def dot(A, x):
    """Matrix multiplication."""
    assert isinstance(A, np.matrix)

    # Ordinary numpy multiplication if possible
    if not isinstance(x, ValJac):
        return A*x

    return ValJac(A*x.val, A*x.jac)

def multiply(x, y):
    """Element-wise multiplication. Similar to .* in Matlab."""

    # Both matrix
    if isinstance(x, np.matrix) and isinstance(y, np.matrix):
        return np.multiply(x, y)

    # Both ValJac
    if isinstance(x, ValJac) and isinstance(y, ValJac):
        assert x.val.shape[0] == y.val.shape[0]
        assert x.val.shape[1] == y.val.shape[1]
        assert x.jac.shape[0] == y.jac.shape[0]
        assert x.jac.shape[1] == y.jac.shape[1]
        return ValJac(np.multiply(x.val, y.val),
                np.multiply(x.val, y.jac) + np.multiply(y.val, x.jac))

    # First matrix, second ValJac
    if isinstance(x, np.matrix) and isinstance(y, ValJac):
        assert x.shape[0] == y.val.shape[0]
        assert x.shape[1] == y.val.shape[1]
        return ValJac(np.multiply(x, y.val), np.multiply(x, y.jac))

    # First ValJac, second matrix
    if isinstance(x, ValJac) and isinstance(y, np.matrix):
        assert x.val.shape[0] == y.shape[0]
        assert x.val.shape[1] == y.shape[1]
        return ValJac(np.multiply(x.val, y), np.multiply(x.jac, y))

    raise NotImplementedError("Only ValJac and np.matrix are supported")

def exp(x):
    """Elementwise exponentiation."""
    if isinstance(x, ValJac):
        ex = np.exp(x.val)
        return ValJac(ex, np.multiply(x.jac, ex))
    else:
        return np.exp(x)

def vstack(l):
    """
    Stack vectors vertically.

    Case 1: No objects are Valjac objects. Simply stack them with Numpy.

    Case 2: Some objects are Valjac objects. In this case the
    non-ValJac-objects are given a zero-jacobian with appropriate dimension.
    """
    # The easy part, the values
    val = np.vstack((obj.val if hasattr(obj, 'val') else obj for obj in l))

    # Ensure the values have the same amount if derivatives
    ns = [obj.jac.shape[1] for obj in l if isinstance(obj, ValJac)]
    if ns:
        assert all(n == ns[0] for n in ns)
        n = ns[0]
    else:
        # No ValJac objects. Simply return the stacked values.
        return val

    def jac_generator(l, n):
        """
        Returns the Jacobian if it exists.
        Otherwise creates a zero Jacobian with appropriate dimensions.
        """
        for obj in l:
            if isinstance(obj, ValJac):
                yield obj.jac
            elif np.isscalar(obj):
                yield np.zeros([1, n])
            else:
                assert obj.shape[0] >= 1
                print(obj.shape[0])
                yield np.zeros([obj.shape[0], n])

    jac = np.vstack(jac_generator(l, n))
    return ValJac(val, jac)

class ValJac:
    """
    Implements forward Automatic Differentiation (AD) using gradient
    arithmetic.

    val
    ---
    Value of function evaluated in a point
    Should be a numpy column matrix of length n.

    jac
    ----
    Jacobian of function evaluated in a point
    Should be a m × n numpy matrix.
    """
    def __init__(self, val, jac=None):
        # We restrict ourselves to matrices for easier code.
        assert isinstance(val, np.matrix)
        assert isinstance(jac, np.matrix) or jac is None

        # Check that val is a column vector, and that the Jacobian has a
        # matching shape
        assert val.shape[1] == 1
        if jac is None:
            self.val = val
            self.jac = np.asmatrix(np.eye(val.shape[0]))
        else:
            assert jac.shape[0] == val.shape[0]
            self.val = val
            self.jac = jac


    def __str__(self):
        """String to be displayed when object is printed"""
        return "(val: %s, jac:\n%s)" % (self.val, self.jac)

    def __repr__(self):
        """String representation of object"""
        return self.__str__()

    def __eq__(self, other):
        """
        Element-wise comparison of value equality.

        Returns a matrix of True/False values.
        """
        try:
            # ValJac object
            return self.val == other.val
        except AttributeError:
            return self.val == other

    def __len__(self):
        """Returns the length of the value vector"""
        return len(self.val)

    def __getitem__(self, i):
        """Returns the value and gradient for some indeval"""
        return ValJac(self.val[i,:], self.jac[i,:])

    def __setitem__(self, i, value):
        assert isinstance(value, ValJac)
        self.val[i] = value.val
        self.jac[i] = value.jac

    def __neg__(self):
        # J(-u) = -Ju
        return ValJac(-self.val, -self.jac)

    def __mul__(self, other):
        """
        Single variable multiplication, or scalar multiplication.
        """
        # Ordinary single-variable multiplication
        if isinstance(other, ValJac):
            assert self.val.shape[0]  == 1 and self.val.shape[1]  == 1
            assert other.val.shape[0] == 1 and other.val.shape[1] == 1
            return ValJac(self.val * other.val,
                          self.val * other.jac + other.val * self.jac)

        # Scalar multiplication
        if np.isscalar(other):
            return ValJac(other*self.val, other*self.jac)

        raise NotImplementedError

    def __rmul__(self, other):
        return self.__mul__(other)

    def __add__(self, other):
        """
        Adds values and jacobians of two objects.

        If one value is scalar, elementwise addition is performed.
        If one object is constant, its Jacobian is assumed to be zero.

        Oops: Put the ValJac object to the left when performing operations with
        other objects. Numpy tries to be smart when performing addition between
        Numpy classes and objects of other classes, causing weird behavior.

        x = ValJac(...)
        y = np.matrix(...)

        x + y # correct
        y + x # incorrect
        """
        # J(u + v) = Ju + Jv
        if np.isscalar(other):
            # This is a scalar, not a ValJac object
            return ValJac(self.val + other, self.jac)

        if isinstance(other, np.matrix):
            # This is a constant vector, not a Valjac object
            assert np.array_equal(self.val.shape, other.shape)
            return ValJac(self.val + other, self.jac)

        if isinstance(other, ValJac):
            return ValJac(self.val + other.val,
                           self.jac + other.jac)

        raise NotImplementedError("Only ValJac, scalar and np.matrix is allowed")

    def __radd__(self, other):
        raise NotImplementedError("Put ValJac to the left")

    def __sub__(self, other):
        return self.__add__(-other)

    def __rsub__(self, other):
        raise NotImplementedError("Put ValJac to the left")

    def __truediv__(self, other):
        """Divide every element by some scalar, x/5."""
        assert np.isscalar(other), "Only scalar division supported"
        return ValJac(self.val / other,
                      self.jac / other)

    def __rtruediv__(self, other):
        """Divide some scalar with every element, 5/x."""
        assert np.isscalar(other)
        return ValJac(other/self.val, np.multiply(-other/np.square(self.val), self.jac))

    def __div__(self, other):
        raise NotImplementedError("Enable Python 3 division")

    def __rdiv__(self, other):
        raise NotImplementedError("Enable Python 3 division")

    def __sin__(self):
        """Take the sine of every element"""
        raise NotImplementedError
        # Jsin(u) = cos(u) * Ju
        return ValJac(sin(self.val),
                       cos(self.val) * self.jac)

    def __cos__(self):
        """Take the cosine of every element"""
        raise NotImplementedError
        # Jcos(u) = -sin(u) * Ju
        return ValJac(cos(self.val),
                       -sin(self.val) * self.jac)

    def __pow__(self, power):
        """
        Raise the value to some power.
        The value must be scalar.
        """
        # J(u**n) = n*u**(n-1) * Ju    (n constant)
        if len(self.val) == 1:
            return ValJac(self.val**power,
                          power * self.val**(power - 1) * self.jac)
        else:
            raise NotImplementedError
